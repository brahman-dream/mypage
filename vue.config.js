module.exports = {
  devServer: {
      // 配置端口号
      port: 7777
  },
  // 关闭eslint检查：否则代码必须严格按照规范来
  lintOnSave: false
}