import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import "../src/assets/iconfont/iconfont.css";
// import BIRDS from 'vanta/src/vanta.birds'
// Vue.use(BIRDS)
Vue.use(ElementUI)
// 导入axios
import * as axios from 'axios'
// 配置请求的根路径
axios.defaults.baseURL = "http://127.0.0.1:10010/"
Vue.prototype.$http = axios

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
