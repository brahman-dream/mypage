import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import UserIndex from '../views/system/UserIndex'
import towindex from '../views/system/towindex'
import threeindex from '../views/system/threeindex'
import oudindex from '../views/system/oudindex'
// import { component } from 'vue/types/umd'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    children:[
      {path: '/system/userindex',component: UserIndex},
      {path: '/system/toeindex',component: towindex},
      {path: '/system/threeindex',component: threeindex},
      {path: '/system/oudindex',component: oudindex}
    ]
  }
  // {
  //   path: '/about',
  //   name: 'about',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  // }
]

const router = new VueRouter({
  routes
})

export default router
