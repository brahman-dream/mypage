# mypage

#### 介绍
Vue制作的个人主页

### 使用的技术栈
Vue+vue-cli+Axios+Three+Vanta+Element-ui+Nginx+Docker

1.基于vue+element-ui实现页面自适应，及多种风格的显示，使用Three.js+Vanta.js实时渲染交互特效

2.axios实现异步获取内容，减少等待时间，加强体验

后端项目地址：[SpringCloud+SpringBoot实现mypage后端](https://gitee.com/brahman-dream/mypage-back-end.git)

### 项目预览
![项目预览](img/image.png)
![项目预览](img/image1.png)
![项目预览](img/image2.png)

### 说明
本项目部署采用docker+nginx

### 参与贡献
梵梦